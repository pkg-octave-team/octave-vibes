disp ("Testing connection with VIBes-viewer");
setenv ("QT_QPA_PLATFORM", "offscreen");
[in, out, pid] = popen2 ("VIBes-viewer");
beginDrawing ()
drawBox ([1, 2, 3, 4; 2, 3, 4, 5]);
endDrawing ()
### Check whether the server consumes the data in the channel
while (stat (sprintf ("%s/.vibes.json", getenv ("HOME"))).size != 0)
  pause(1);
endwhile
kill (pid, 9);
